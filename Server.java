
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//import org.jsoup.helper.Validate;

public class Server {

	private static ServerSocket s = null;
	private static Socket socket = null;
	private static int conn = 0;

	public static void main(String[] args) {
		if (args.length == 1) {
			// TODO Auto-generated method stub

			// Validate.isTrue(args.length>=2, "Error: Parameters Error");
			 String arg = args[0];
			int port = 8080;
			try {
				port = Integer.parseInt(arg);
			} catch (Exception e) {
				System.err.println("Parameters Error,use default port 8080");
			}
			ThreadsPool.StartWorkThread();
			System.out.println("Server start ....");
			try {
				s = new ServerSocket(port);
				while (true) {
					// System.out.println(ThreadsPool.isRunning() +
					// "aaaaaaaaaa");

					if (ThreadsPool.isRunning()) {
						socket = s.accept();
						conn++;
						Task task = new Task(socket);
						TaskQueue.execute(task);
					} else {

						break;
					}
				}

			} catch (Exception e) {
				try {
					s.close();
					socket.close();
				} catch (IOException e1) {
					//e1.printStackTrace();
				}
			}
		}else{
			System.out.println("Parameters Error");
		}
	}

	public static int getConn() {
		return conn;
	}

	public static void closeAll() {
		try {
			s.close();
			socket.close();
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

}
