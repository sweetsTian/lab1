
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Task implements Runnable {

	private Socket socket = null;
	private String clientSentence, changedSentence;
	private BufferedReader inFromClient = null;
	private DataOutputStream outToClient = null;

	public Task(Socket socket) {
		super();
		this.socket = socket;
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		// TODO Auto-generated method stub

		try {
			if (socket.isClosed()) {
				socket=null;
			} else {
				inFromClient = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				outToClient = new DataOutputStream(socket.getOutputStream());
				clientSentence = inFromClient.readLine();
				if (clientSentence.equals("KILL_SERVICE")) {
					System.out.println("Kill Server");
					inFromClient.close();
					outToClient.close();
					socket.close();
					ThreadsPool.destroy();

					// break;
				} else if (clientSentence.equals("HELO text")) {
					changedSentence = clientSentence  + '\n' + "IP: "
							+ socket.getInetAddress() + '\n' + "Port: "
							+ socket.getPort() + '\n' + "StuentID:"
							+ "14307629" + '\n';


					outToClient.writeBytes(changedSentence);
					outToClient.flush();

				} else {
					changedSentence = clientSentence.toUpperCase() + '\n';
					outToClient.writeBytes(changedSentence);
					outToClient.flush();
					try {
						Thread.currentThread().sleep(50);
					} catch (InterruptedException ie) {
						
					}
				}
				ThreadsPool.adjust();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			try {

				inFromClient.close();
				outToClient.close();
				socket.close();

			} catch (IOException e1) {
				
			}

		} finally {
			try {

				// inFromClient.close();
				// outToClient.close();
				// socket.close();
				if (socket != null) {
					TaskQueue.execute(new Task(socket));
				}

			} catch (Exception e1) {
				
			}
		}

	}
	// }

}
