
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * To store the task(Runnable)
 * @author Tian
 */
public class TaskQueue {
	
	private static BlockingQueue<Runnable> taskQueue = new 
			LinkedBlockingQueue<Runnable>();
	//private static int task_number=0;
	
	/**
	 * Put a task to the taskQueue and wait idle thread to do it
	 */
	public static void execute(Runnable task){
		synchronized (taskQueue) {
			//System.out.println("add task~~~~~~~");
			//System.out.println("task_number is " + task_number+ "then add one");
			//task_number++;
			taskQueue.offer(task);
			taskQueue.notify();
		}
	}
	
	public static Runnable poll() {
		// TODO Auto-generated method stub
		//System.out.println("poll task~~~~~~~~~~~~");
		return taskQueue.poll();
	}
	
	public static void clear() {
		// TODO Auto-generated method stub
		taskQueue.clear();
	}
	
	public static boolean isEmpty() {
		// TODO Auto-generated method stub

		return taskQueue.isEmpty();
	}

	public static void waits(int i) throws InterruptedException{
		try{
			taskQueue.wait(i);
		}catch(Exception e){

		}
	}
	
//	public static int size(){
//		return task_number;
//	}
	
}
