
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

//import org.jsoup.helper.Validate;

public class Client {

	private static String sentence;
	private static DataOutputStream outToServer = null;
	private static BufferedReader inFromServer = null;
	private static BufferedReader inFromUser = null;
	private static Socket clientSocket = null;
	private static String output = null;

	public static void main(String[] args) throws IOException {
		if (args.length == 1) {

			// Validate.isTrue(args.length>=2, "Error: Parameters Error");
			// TODO Auto-generated method stub
			// Socket clientSocket = null;
			String arg = args[0];
			int port = 8080;
			try {
				 port = Integer.parseInt(arg);
			} catch (Exception e) {
				System.err.println("Parameters Error, use default port 8080");
			}
			try {

				clientSocket = new Socket("127.0.0.1", port);
				if (clientSocket.isInputShutdown()) {

					System.out.println("Server is closed....");
				} else {
					Thread sendMsg = new Thread() {
						boolean isRunning = true;

						public void run() {
							try {
								outToServer = new DataOutputStream(
										clientSocket.getOutputStream());
								while (isRunning) {
									inFromUser = new BufferedReader(
											new InputStreamReader(System.in));
									sentence = inFromUser.readLine();
									if (!sentence.equals(null)) {
										outToServer.writeBytes(sentence + '\n');
									}

								}
							} catch (Exception e) {
								System.out.println("Sorry server is closed...");
							}
						}
					};

					Thread receiveMsg = new Thread() {
						boolean isRunning = true;

						public void run() {
							while (isRunning) {

								// System.out.println("test brfore syn");
								// synchronized (Client.class) {

								// System.out.println("test after syn");
								try {

									inFromServer = new BufferedReader(
											new InputStreamReader(
													clientSocket
															.getInputStream()));

									if (!inFromServer.equals(null)) {
										while ((output = inFromServer
												.readLine()) != null) {
											try {
												System.out.println(output);
											} catch (Exception e) {
												// inFromServer.close();
												System.out
														.println("Exception~~~~~");
												break;
											}
											// Do stuff with line
										}
									}

									// Do stuff with line

								} catch (Exception e) {
									System.out
											.println("Sorry server is closed...");
									break;
								}
							}
						}
					};

					sendMsg.start();
					receiveMsg.start();
				}

			} catch (Exception e) {
				System.out.println("Port Error....");
			}

		} else {
			System.err.println("Parameters Error");
		}
	}

}
